<?php
/**
 * The main template file
 */

get_header(); ?>

	<?php if (have_posts()) : ?>
		<?php while (have_posts()) : ?>
			<?php the_post(); ?>

			<?php if (is_blog()) : ?>
				<div id="content" class="container">
					<div class="row">
						<h1>Blog</h1>
					</div>
				</div>
				<?php get_template_part( 'template-parts/content', 'posts' ); ?>
			<?php else : ?>
				<div id="content" class="container">
					<?php the_content(); ?>
				</div>
			<?php endif; ?>
		<?php endwhile; ?>
	<?php endif; ?>

<?php get_footer(); ?>