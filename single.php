<?php
/**
 * The template for displaying single posts
 */

get_header(); ?>

	<div id="content">

		<?php if (have_posts()): ?>
			<?php while (have_posts()): ?>
				<?php the_post(); ?>
				<div class="row">
					<div class="col-md-12">
						<?php /* if (has_post_thumbnail()) : ?>
							<?php the_post_thumbnail('full', ['class'=>'img-responsive']); ?>
						<?php endif; */ ?>
						<h1><?php the_title(); ?></h1>
						<?php the_content(); ?>
					</div>
				</div>
			<?php endwhile; ?>
		<?php endif; ?>

	</div>

<?php get_footer(); ?>