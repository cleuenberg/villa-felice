jQuery(document).ready(function($) {
	//open featherlight on meteor slide link:
	if ($('.meteor-clip a[title=Video]').length ) {
		var videoSrc = $('.meteor-clip a[title=Video]').attr('href');
		$('.meteor-clip a[title=Video]').featherlight(
			{
				iframe: videoSrc,
				iframeWidth: 960,
				iframeHeight: 540,
				iframeAllowFullScreen: true
			}
		);
	}

	//init Flexslider
	$(window).load(function() {
		// The slider being synced must be initialized first
		$('#carousel').flexslider({
			animation: "slide",
			controlNav: false,
			animationLoop: false,
			slideshow: false,
			itemWidth: 150,
			itemMargin: 1,
			asNavFor: '#slider'
		});

		$('#slider').flexslider({
			smoothHeight: true,
			animation: "slide",
			animationSpeed: 300,
			controlNav: false,
			animationLoop: false,
			slideshow: false,
			sync: "#carousel"
		});
	});

	//Tabs on Escort Map
	$('#escort-map area').click(function (e) {
		e.preventDefault()
		$(this).tab('show')
	})
});