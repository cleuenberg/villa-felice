<?php
/**
 * The standard template for displaying pages
 */

get_header(); ?>

	<?php if (have_posts()): ?>
		<?php while (have_posts()): ?>
			<div id="content">
				<?php the_post(); ?>
				<div class="row">
					<div class="col-md-12">
						<h1><?php the_title(); ?></h1>
						<h2><?php echo date_i18n( 'l' ); ?><small><?php echo date_i18n( 'd.m.Y' ); ?></small></h2>
						<?php the_content(); ?>

						<?php
						//Tagesplan
						//query available
						$query_available_args = array(
							'post_type'  => 'girls',
							'posts_per_page' => -1,
							'meta_query' => array(
								'availability_clause' => array(
					            	'key'     => 'availability',
					            	'value'	  => 'available',
					            	'compare' => '='
					            ),
					            array(
									'key'     => 'show_on_home',
									'value'   => '"yes"',
									'compare' => 'LIKE'
								),
					        ),
					        'orderby' => array(
					        	'title' => 'ASC'
					        )
					    );
					    $query_available = new WP_Query( $query_available_args );

					    //query available_from
						$query_available_from_args = array(
							'post_type'  => 'girls',
							'posts_per_page' => -1,
							'meta_query' => array(
								'availability_clause' => array(
					            	'key'     => 'availability',
					            	'value'	  => 'available_from',
					            	'compare' => '='
					            ),
					            'availability_date_clause' => array(
						            'key'     => 'available_from',
						            'type'	  => 'DATETIME',
						            'compare' => 'EXISTS',
						        ),
						        array(
									'key'     => 'show_on_home',
									'value'   => '"yes"',
									'compare' => 'LIKE'
								),
					        ),
					        'orderby' => array(
					        	'availability_date_clause' => 'ASC',
					        	'title' => 'ASC'
					        )
					    );
					    $query_available_from = new WP_Query( $query_available_from_args );

						if ($query_available->have_posts()) : ?>

							<?php if (!current_user_can('administrator') && check_visitor_origin()) : ?>

								<?php get_template_part( 'template-parts/content', 'blocked' ); ?>

							<?php else : ?>
						
								<div class="row" id="girls">
									<?php while ( $query_available->have_posts() ) : $query_available->the_post(); ?>
								        <div class="col-sm-4 col-md-3">
											<div class="thumbnail">
												<?php if (get_field('status')) : ?>
													<?php $girl_status = get_field_object('status'); $girl_status_value = $girl_status['value']; $girl_status_label = $girl_status['choices'][$girl_status_value]; ?>
													<span class="status_badge <?php echo $girl_status_value; ?>"><?php echo $girl_status_label; ?></span>
												<?php endif; ?>
												<a href="<?php the_permalink(); ?>"><?php echo get_the_post_thumbnail( $post->ID, 'tagesplan' ); ?></a>
												<div class="caption">
													<h3>
														<a href="<?php the_permalink(); ?>"><?php the_title(); ?></a>
														<?php if (get_field('availability') == 'available') : ?>
															<span class="label label-success">verfügbar</span>
														<?php elseif (get_field('availability') == 'available_from') : 
															$available_from_date = get_field('available_from', false, false);
															$available_from_date = new DateTime($available_from_date); ?><span class="label label-warning">ab <?php echo $available_from_date->format('d.m.Y'); ?></span>
														<?php elseif (get_field('availability') == 'unavailable') : ?>
															<span class="label label-danger">nicht verfügbar</span>
														<?php endif; ?>
													</h3>
												</div>
											</div>
										</div>
									<?php endwhile; ?>
									<?php wp_reset_postdata(); ?>

									<?php while ( $query_available_from->have_posts() ) : $query_available_from->the_post(); ?>
								        <div class="col-sm-4 col-md-3">
											<div class="thumbnail">
												<?php if (get_field('status')) : ?>
													<?php $girl_status = get_field_object('status'); $girl_status_value = $girl_status['value']; $girl_status_label = $girl_status['choices'][$girl_status_value]; ?>
													<span class="status_badge <?php echo $girl_status_value; ?>"><?php echo $girl_status_label; ?></span>
												<?php endif; ?>
												<a href="<?php the_permalink(); ?>"><?php echo get_the_post_thumbnail( $post->ID, 'tagesplan' ); ?></a>
												<div class="caption">
													<h3>
														<a href="<?php the_permalink(); ?>"><?php the_title(); ?></a>
														<?php if (get_field('availability') == 'available') : ?>
															<span class="label label-success">verfügbar</span>
														<?php elseif (get_field('availability') == 'available_from') : 
															$available_from_date = get_field('available_from', false, false);
															$available_from_date = new DateTime($available_from_date); ?><span class="label label-warning">ab <?php echo $available_from_date->format('d.m.Y'); ?></span>
														<?php elseif (get_field('availability') == 'unavailable') : ?>
															<span class="label label-danger">nicht verfügbar</span>
														<?php endif; ?>
													</h3>
												</div>
											</div>
										</div>
									<?php endwhile; ?>
									<?php wp_reset_postdata(); ?>
								</div>

							<?php endif; ?>
						<?php endif; ?>

					</div>
				</div>
			</div>
		<?php endwhile; ?>
	<?php endif; ?>

<?php get_footer(); ?>
