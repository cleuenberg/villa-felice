<?php
/**
 * The template for displaying the footer
 */
?>
    </div><!-- /.container -->

    <footer class="footer">
		<div class="container">
			<p><img alt="<?php echo get_bloginfo('name'); ?>" src="<?php the_field('logo_footer', 'option'); ?>" class="img-responsive"></p>
			<div class="row">
				<div class="col-md-4">
					<?php the_field('column1', 'option'); ?>
				</div>
				<div class="col-md-4">
					<?php the_field('column2', 'option'); ?>
				</div>
				<div class="col-md-4">
					<?php
						wp_nav_menu( array(
							'theme_location' => 'footer',
							'menu_class'     => 'nav nav-pills pull-right'
						) );
					?>
					<p><strong>Folgen Sie uns</strong></p>
					<ul class="list-inline">
						<?php if (get_field('facebook', 'option')) : ?><li><a href="<?php the_field('facebook', 'option'); ?>" target="_blank"><i class="fa fa-2x fa-facebook-square"></i></a></li><?php endif; ?>
						<?php if (get_field('youtube', 'option')) : ?><li><a href="<?php the_field('youtube', 'option'); ?>" target="_blank"><i class="fa fa-2x fa-youtube-square"></i></a></li><?php endif; ?>
						<?php if (get_field('vimeo', 'option')) : ?><li><a href="<?php the_field('vimeo', 'option'); ?>" target="_blank"><i class="fa fa-2x fa-vimeo-square"></i></a></li><?php endif; ?>
						<?php if (get_field('twitter', 'option')) : ?><li><a href="<?php the_field('twitter', 'option'); ?>" target="_blank"><i class="fa fa-2x fa-twitter-square"></i></a></li><?php endif; ?>
						<?php if (get_field('instagram', 'option')) : ?><li><a href="<?php the_field('instagram', 'option'); ?>" target="_blank"><i class="fa fa-2x fa-instagram"></i></a></li><?php endif; ?>
						<?php if (get_field('googleplus', 'option')) : ?><li><a href="<?php the_field('googleplus', 'option'); ?>" target="_blank"><i class="fa fa-2x fa-google-plus-square"></i></a></li><?php endif; ?>
					</ul>
					<div class="seperator seperator-small"></div>
					<p><strong>Newsletter</strong></p>
					<form class="form-inline">
						<div class="form-group">
							<input type="email" class="form-control" placeholder="Ihre E-Mail">
						</div>
						<button type="submit" class="btn btn-default btn-felice">abonnieren</button>
					</form>
				</div>
			</div>
		</div>
	</footer>

<?php wp_footer(); ?>
</body>
</html>