<?php
/**
 * The template for displaying single girls
 */

get_header(); ?>

	<div id="content">

		<?php if (!current_user_can('administrator') && check_visitor_origin()) : ?>

			<?php get_template_part( 'template-parts/content', 'blocked' ); ?>

		<?php else : ?>

			<?php if (have_posts()): ?>
				<?php while (have_posts()): ?>
					<?php the_post(); ?>
					<div class="row">
					<?php if (has_post_thumbnail()) : ?>
						<div class="col-md-4 girl-mainphoto">
							<?php the_post_thumbnail('full', ['class'=>'img-responsive']); ?>
						</div>
						<div class="col-md-8">
					<?php else : ?>
						<div class="col-md-12">
					<?php endif; ?>
							<h1>
								<?php the_title(); ?>
								<?php if (get_field('availability') == 'available') : ?>
									<span class="label label-success">aktuell verfügbar</span>
								<?php elseif (get_field('availability') == 'available_from') : 
									$available_from_date = get_field('available_from', false, false);
									$available_from_date = new DateTime($available_from_date); ?><span class="label label-warning">verfügbar ab <?php echo $available_from_date->format('d.m.Y'); ?></span>
								<?php elseif (get_field('availability') == 'unavailable') : ?>
									<span class="label label-danger">nicht verfügbar</span>
								<?php endif; ?>
							</h1>
							<?php the_content(); ?>
							<hr>
							<h4>Meine Daten</h4>
							<table class="table table-striped">
								<tbody>
									<tr>
										<td style="width:20%;"><strong>Herkunft</strong></td>
										<td><?php the_field('herkunft'); ?></td>
									</tr>
									<tr>
										<td><strong>Grösse</strong></td>
										<td><?php the_field('groesse'); ?>m</td>
									</tr>
									<tr>
										<td><strong>Oberweite</strong></td>
										<td><?php the_field('oberweite'); ?></td>
									</tr>
									<tr>
										<td><strong>Konfektion</strong></td>
										<td><?php the_field('konfektion'); ?></td>
									</tr>
									<tr>
										<td><strong>Frisur</strong></td>
										<td><?php the_field('frisur'); ?></td>
									</tr>
									<tr>
										<td><strong>Haarfarbe</strong></td>
										<td><?php the_field('haarfarbe'); ?></td>
									</tr>
									<tr>
										<td><strong>Alter</strong></td>
										<td><?php the_field('alter'); ?> Jahre</td>
									</tr>
								</tbody>
							</table>
							<hr>
							<h4>Mein Service</h4>
							<?php
							// vars	
							$services = get_field('service');

							// check
							if( $services ): ?>
							<ul class="list-unstyled list-inline">
								<?php foreach( $services as $service ): ?>
									<li><i class="fa fa-check" aria-hidden="true"></i> <?php echo $service; ?></li>
								<?php endforeach; ?>
							</ul>
							<?php endif; ?>
						</div>
					</div>
					<div class="row">
							<?php
							$images = get_field('galerie');

							if( $images ): ?>
								<h2>Bildergalerie</h2>

								<div class="col-md-6 col-md-offset-3">
								    <div id="slider" class="flexslider">
								        <ul class="slides">
								            <?php foreach( $images as $image ): ?>
								                <li>
								                    <img src="<?php echo $image['sizes']['large']; ?>" alt="<?php echo $image['alt']; ?>" />
								                </li>
								            <?php endforeach; ?>
								        </ul>
								    </div>
								</div>
								<div class="col-md-12">
								    <div id="carousel" class="flexslider">
								        <ul class="slides">
								            <?php foreach( $images as $image ): ?>
								                <li>
								                    <img src="<?php echo $image['sizes']['galerie_thumb']; ?>" alt="<?php echo $image['alt']; ?>" />
								                </li>
								            <?php endforeach; ?>
								        </ul>
								    </div>
								</div>
							<?php endif; ?>
						</div>
					</div>
				<?php endwhile; ?>
			<?php endif; ?>

		<?php endif; ?>

	</div>

<?php get_footer(); ?>