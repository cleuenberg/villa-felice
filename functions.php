<?php
/**
 * Enqueues scripts and styles.
 *
 */
function felice_scripts() {
	// Bootstrap stylesheet
	wp_enqueue_style( 'bootstrap-style', get_template_directory_uri() . '/css/bootstrap.min.css' );

	// FontAwesome stylesheet
	wp_enqueue_style( 'fontawesome-style', get_template_directory_uri() . '/css/font-awesome.min.css' );

	// Featherlight stylesheet
	wp_enqueue_style( 'featherlight-style', get_template_directory_uri() . '/css/featherlight.min.css' );

	// Flexslider stylesheet
	wp_enqueue_style( 'flexslider-style', get_template_directory_uri() . '/css/flexslider.css' );

	// Theme stylesheet
	wp_enqueue_style( 'felice-style', get_stylesheet_uri() );

	// Bootstrap scripts
	wp_enqueue_script( 'bootstrap-script', get_template_directory_uri() . '/js/bootstrap.min.js', array( 'jquery' ), '20170215', true );

	// Featherlight scripts
	wp_enqueue_script( 'featherlight-script', get_template_directory_uri() . '/js/featherlight.min.js', array( 'jquery' ), '1.7.1', true );

	// Flexslider scripts
	wp_enqueue_script( 'flexslider-script', get_template_directory_uri() . '/js/jquery.flexslider-min.js', array( 'jquery' ), '2.6.3', true );

	// Theme scripts
	wp_enqueue_script( 'felice-script', get_template_directory_uri() . '/js/functions.js', array( 'jquery' ), '20170215', true );
}
add_action( 'wp_enqueue_scripts', 'felice_scripts' );


// Registering the menus
register_nav_menus( array(
	'main' => __( 'Main Menu', 'felice' ),
	'footer' => __( 'Footer Menu', 'felice' ),
) );

// Registering the widgets
function felice_widgets_init() {
	register_sidebar( array(
		'name'          => 'Sidebar',
		'id'            => 'sidebar-1',
		'description'   => '',
		'before_widget' => '<section id="%1$s" class="widget %2$s">',
		'after_widget'  => '</section>',
		'before_title'  => '<h2 class="widget-title">',
		'after_title'   => '</h2>',
	));
	register_sidebar( array(
		'name'          => 'Sidebar Home',
		'id'            => 'sidebar-home',
		'description'   => '',
		'before_widget' => '<section id="%1$s" class="widget %2$s">',
		'after_widget'  => '</section>',
		'before_title'  => '<h2 class="widget-title">',
		'after_title'   => '</h2>',
	));
}
add_action( 'widgets_init', 'felice_widgets_init' );

// add image sizes
add_image_size( 'tagesplan', 400, 600, true );
add_image_size( 'galerie_thumb', 480, 480, true );

// Remove WP version number
function felice_remove_version() {
	return '';
}
add_filter('the_generator', 'felice_remove_version');

// Add ACF Theme Options
if( function_exists('acf_add_options_page') ) {
	acf_add_options_page();
}

// Chech if blog page
function is_blog () {
    return ( is_archive() || is_author() || is_category() || is_home() || is_single() || is_tag()) && 'post' == get_post_type();
}

// Shortcode Map
function felice_sc_map( $atts ) {
	// attributes
	extract(shortcode_atts(
		array(
			'columns' => 2
		), $atts )
	);

	$html = '
	<div class="row">
		<div class="col-md-7">
			<img src="'.get_template_directory_uri().'/img/map.png" alt="Escort-Service Karte" usemap="#escort-map">
			<map name="escort-map" id="escort-map" role="tablist">
			    <area href="#escort-BS" aria-controls="escort-BS" role="tab" data-toggle="tab" name="escort-BS" title="BS" shape="poly" coords="142,52,147,56,156,54,163,44,172,37,182,30,188,29,195,33,200,34,210,29,218,33,225,34,233,33,240,28,247,23,254,23,263,28,271,33,275,29,270,23,269,11,274,5,262,1,247,3,229,3,212,5,184,7,172,7,158,7,145,10,130,13,120,15,110,23,100,34,95,41,93,54,97,56,109,51,120,44,133,46">
			    <area href="#escort-AR" aria-controls="escort-AR" role="tab" data-toggle="tab" name="escort-AR" title="AR" shape="poly" coords="358,87,363,75,369,69,374,69,390,65,400,61,396,67,390,70,374,75,370,80,368,87,374,91,367,91">
			    <area href="#escort-AI" aria-controls="escort-AI" role="tab" data-toggle="tab" name="escort-AI" title="AI" shape="poly" coords="381,74,390,78,393,79,390,85,387,90,381,92,374,87,376,81">
			    <area href="#escort-SG" aria-controls="escort-SG" role="tab" data-toggle="tab" name="escort-SG" title="SG" shape="poly" coords="402,53,393,59,380,66,367,66,357,69,356,76,354,87,355,92,364,95,376,99,388,96,394,90,401,75,408,66,408,72,403,83,394,99,394,108,394,116,394,122,396,131,396,136,390,142,390,149,382,148,371,145,368,136,367,124,363,117,361,108,353,106,347,103,341,97,330,92,336,85,339,76,339,69,345,61,350,58,363,58,374,56,383,56,390,51,396,45">
			    <area href="#escort-GL" aria-controls="escort-GL" role="tab" data-toggle="tab" name="escort-GL" title="GL" shape="poly" coords="340,106,350,112,357,115,356,123,354,129,362,131,363,135,364,145,357,153,352,154,346,153,342,159,335,165,330,159,339,152,339,140,335,132,334,124,337,117">
			    <area href="#escort-TG" aria-controls="escort-TG" role="tab" data-toggle="tab" name="escort-TG" title="TG" shape="poly" coords="389,40,383,51,375,46,369,48,362,53,354,51,344,52,338,56,335,65,328,59,330,51,327,42,320,37,316,33,318,25,327,25,337,25,348,23,360,28,367,27,374,30,381,36">
			    <area href="#escort-SH" aria-controls="escort-SH" role="tab" data-toggle="tab" name="escort-SH" title="SH" shape="poly" coords="284,23,277,19,282,10,288,5,294,3,302,5,307,9,308,15,300,18,296,22">
			    <area href="#escort-ZH" aria-controls="escort-ZH" role="tab" data-toggle="tab" name="escort-ZH" title="ZH" shape="poly" coords="303,95,312,91,322,85,330,83,330,76,326,66,320,56,323,50,316,44,312,37,307,30,302,28,298,33,293,39,287,34,284,41,277,45,274,50,272,54,276,63,273,68,278,75,278,82,274,91,280,89,290,90">
			    <area href="#escort-SZ" aria-controls="escort-SZ" role="tab" data-toggle="tab" name="escort-SZ" title="SZ" shape="poly" coords="334,100,334,110,330,120,327,125,329,135,331,140,324,145,316,140,304,138,293,133,278,122,286,119,294,122,299,122,308,117,309,110,308,103,315,101,322,101">
			    <area href="#escort-ZG" aria-controls="escort-ZG" role="tab" data-toggle="tab" name="escort-ZG" title="ZG" shape="poly" coords="276,109,285,115,295,117,300,115,303,109,299,103,291,96,281,96,276,99">
			    <area href="#escort-NW" aria-controls="escort-NW" role="tab" data-toggle="tab" name="escort-NW" title="NW" shape="poly" coords="274,122,270,132,267,139,269,151,275,153,279,147,284,139,287,136,282,129">
			    <area href="#escort-UR" aria-controls="escort-UR" role="tab" data-toggle="tab" name="escort-UR" title="UR" shape="poly" coords="280,207,284,197,295,199,300,199,300,188,302,183,310,176,316,170,322,163,326,156,330,151,319,149,310,146,300,146,294,141,288,148,284,153,284,163,283,171,280,178,278,186,276,192,277,199">
			    <area href="#escort-OW" aria-controls="escort-OW" role="tab" data-toggle="tab" name="escort-OW" title="OW" shape="poly" coords="254,138,245,141,241,151,236,162,240,169,252,173,260,171,263,166,263,156,263,147,262,138">
			    <area href="#escort-LU" aria-controls="escort-LU" role="tab" data-toggle="tab" name="escort-LU" title="LU" shape="poly" coords="225,92,222,99,214,102,214,112,217,121,216,127,223,132,224,136,221,145,216,152,219,160,225,166,231,167,233,157,234,147,242,138,250,133,260,127,267,122,272,115,263,107,258,99,255,91,250,96,242,92,236,92">
			    <area href="#escort-AG" aria-controls="escort-AG" role="tab" data-toggle="tab" name="escort-AG" title="AG" shape="poly" coords="207,41,217,40,225,46,234,42,245,39,250,34,257,33,263,41,270,42,266,51,269,61,269,68,272,76,272,84,269,91,271,101,271,106,263,94,262,85,255,82,250,85,244,87,234,87,229,85,220,87,217,92,210,90,217,81,228,78,234,73,234,63,228,56,220,52,216,47">
			    <area href="#escort-BL" aria-controls="escort-BL" role="tab" data-toggle="tab" name="escort-BL" title="BL" shape="poly" coords="193,70,201,69,212,65,215,56,211,48,201,48,194,46,186,44,178,44,173,47,167,56,163,60,159,62,162,66,170,61,177,58,178,51,187,48,195,54,192,61">
			    <area href="#escort-SO" aria-controls="escort-SO" role="tab" data-toggle="tab" name="escort-SO" title="SO" shape="poly" coords="158,100,162,103,170,103,170,108,164,115,168,115,175,107,183,106,187,102,182,94,178,89,184,84,195,84,206,85,212,79,225,75,227,66,219,65,208,73,200,78,188,73,187,65,191,56,185,56,178,65,170,68,178,79,178,84,172,89,164,92">
			    <area href="#escort-JU" aria-controls="escort-JU" role="tab" data-toggle="tab" name="escort-JU" title="JU" shape="poly" coords="104,101,114,100,119,90,127,89,134,84,143,82,148,84,156,81,165,81,172,81,163,74,154,67,145,66,134,63,134,56,124,53,119,55,116,62,107,68,118,67,126,68,126,76,118,80,112,88">
			    <area href="#escort-BE" aria-controls="escort-BE" role="tab" data-toggle="tab" name="escort-BE" title="BE" shape="poly" coords="144,236,155,234,165,230,174,228,183,223,193,225,206,220,218,210,225,202,242,206,253,206,260,202,266,193,267,183,268,175,255,182,242,178,228,174,215,168,208,162,208,151,212,139,208,129,210,112,207,100,195,92,188,92,194,105,188,112,178,116,172,122,168,127,158,123,155,112,151,105,157,92,146,92,137,93,124,102,114,110,106,111,129,118,131,125,127,130,140,130,147,134,146,145,155,152,157,161,155,170,158,178,163,185,160,193,155,202,150,216">
			    <area href="#escort-NE" aria-controls="escort-NE" role="tab" data-toggle="tab" name="escort-NE" title="NE" shape="poly" coords="92,152,106,140,119,129,122,122,117,118,105,122,100,112,92,119,86,124,80,133,73,138,63,141,59,146,59,153,65,154,78,142,88,141">
			    <area href="#escort-FR" aria-controls="escort-FR" role="tab" data-toggle="tab" name="escort-FR" title="FR" shape="poly" coords="99,194,104,203,114,212,120,216,130,208,139,203,148,193,153,186,146,179,145,170,145,161,144,153,135,149,135,141,127,140,127,149,129,154,124,163,116,163,112,170,112,177,112,183,104,186">
			    <area href="#escort-VD" aria-controls="escort-VD" role="tab" data-toggle="tab" name="escort-VD" title="VD" shape="poly" coords="22,230,22,212,25,203,46,181,57,173,60,163,73,154,80,152,87,166,92,173,102,176,104,179,94,189,91,199,95,206,92,212,97,218,106,218,111,225,121,226,134,218,141,210,137,225,137,240,138,245,131,257,120,259,114,249,109,236,99,227,81,218,68,216,53,223,39,228,34,234">
			    <area href="#escort-GE" aria-controls="escort-GE" role="tab" data-toggle="tab" name="escort-GE" title="GE" shape="poly" coords="26,249,31,257,28,261,20,268,9,271,1,265,6,259,15,253">
			    <area href="#escort-VS" aria-controls="escort-VS" role="tab" data-toggle="tab" name="escort-VS" title="VS" shape="poly" coords="272,201,267,210,262,220,253,234,240,242,232,251,238,265,232,273,225,285,215,295,202,300,190,296,175,292,162,300,151,305,133,307,120,294,112,277,101,265,104,251,112,262,122,268,136,263,148,253,155,245,167,240,177,242,186,236,197,234,208,230,219,223,228,216,237,214,248,216,255,214">
			    <area href="#escort-TI" aria-controls="escort-TI" role="tab" data-toggle="tab" name="escort-TI" title="TI" shape="poly" coords="274,216,291,207,308,207,326,208,336,201,337,212,344,225,344,242,345,258,350,266,345,275,339,283,337,292,338,302,342,314,332,307,324,294,327,279,320,271,312,273,300,271,293,259,285,251,286,234,284,223">
			    <area href="#escort-GR" aria-controls="escort-GR" role="tab" data-toggle="tab" name="escort-GR" title="GR" shape="poly" coords="307,184,370,153,399,157,405,145,406,129,431,135,438,151,456,159,471,165,487,154,495,144,500,153,493,168,491,179,490,190,501,200,490,203,479,189,468,191,457,200,451,214,453,223,462,236,467,255,455,245,451,234,442,230,423,238,415,245,404,242,395,230,396,216,383,212,370,214,366,230,369,240,362,260,353,251,355,232,354,216,349,210,343,197,331,188,327,195,313,199">
			</map>
		</div>
		<div class="col-md-5">
			<!-- Tab panes -->
			<div class="tab-content">
				<div role="tabpanel" class="tab-pane active" id="escort-info">
					<h4>Preise</h4>
					<p>Klicken Sie auf Ihr Kanton in der Karte, um den Preis für unseren Escort-Service zu erfahren.</p>
				</div>
				<div role="tabpanel" class="tab-pane" id="escort-BS">
					<h4>Preise Basel</h4>';

if(get_field('kanton_bs')) :
	$html .= '<table class="table table-striped" border="0">';
	while(the_repeater_field('kanton_bs')) :
        $html .= '<tr><td>' . get_sub_field('escort_service') . '</td><td class="text-right">' . get_sub_field('escort_preis') . '</td></tr>';
    endwhile;
    $html .= '</table>';
endif;


	$html .= '
				</div>

				<div role="tabpanel" class="tab-pane" id="escort-AR">
					<h4>Preise Appenzell Ausserrhoden</h4>';

if(get_field('kanton_ar')) :
	$html .= '<table class="table table-striped" border="0">';
	while(the_repeater_field('kanton_ar')) :
        $html .= '<tr><td>' . get_sub_field('escort_service') . '</td><td class="text-right">' . get_sub_field('escort_preis') . '</td></tr>';
    endwhile;
    $html .= '</table>';
endif;


	$html .= '
				</div>

				<div role="tabpanel" class="tab-pane" id="escort-AI">
					<h4>Preise Appenzell</h4>';

if(get_field('kanton_ai')) :
	$html .= '<table class="table table-striped" border="0">';
	while(the_repeater_field('kanton_ai')) :
        $html .= '<tr><td>' . get_sub_field('escort_service') . '</td><td class="text-right">' . get_sub_field('escort_preis') . '</td></tr>';
    endwhile;
    $html .= '</table>';
endif;


	$html .= '
				</div>

				<div role="tabpanel" class="tab-pane" id="escort-SG">
					<h4>Preise St.Gallen</h4>';

if(get_field('kanton_sg')) :
	$html .= '<table class="table table-striped" border="0">';
	while(the_repeater_field('kanton_sg')) :
        $html .= '<tr><td>' . get_sub_field('escort_service') . '</td><td class="text-right">' . get_sub_field('escort_preis') . '</td></tr>';
    endwhile;
    $html .= '</table>';
endif;


	$html .= '
				</div>

				<div role="tabpanel" class="tab-pane" id="escort-GL">
					<h4>Preise Glarus</h4>';

if(get_field('kanton_gl')) :
	$html .= '<table class="table table-striped" border="0">';
	while(the_repeater_field('kanton_gl')) :
        $html .= '<tr><td>' . get_sub_field('escort_service') . '</td><td class="text-right">' . get_sub_field('escort_preis') . '</td></tr>';
    endwhile;
    $html .= '</table>';
endif;


	$html .= '
				</div>

				<div role="tabpanel" class="tab-pane" id="escort-TG">
					<h4>Preise Thurgau</h4>';

if(get_field('kanton_tg')) :
	$html .= '<table class="table table-striped" border="0">';
	while(the_repeater_field('kanton_tg')) :
        $html .= '<tr><td>' . get_sub_field('escort_service') . '</td><td class="text-right">' . get_sub_field('escort_preis') . '</td></tr>';
    endwhile;
    $html .= '</table>';
endif;


	$html .= '
				</div>

				<div role="tabpanel" class="tab-pane" id="escort-SH">
					<h4>Preise Schaffhausen</h4>';

if(get_field('kanton_sh')) :
	$html .= '<table class="table table-striped" border="0">';
	while(the_repeater_field('kanton_sh')) :
        $html .= '<tr><td>' . get_sub_field('escort_service') . '</td><td class="text-right">' . get_sub_field('escort_preis') . '</td></tr>';
    endwhile;
    $html .= '</table>';
endif;


	$html .= '
				</div>

				<div role="tabpanel" class="tab-pane" id="escort-ZH">
					<h4>Preise Zürich</h4>';

if(get_field('kanton_zh')) :
	$html .= '<table class="table table-striped" border="0">';
	while(the_repeater_field('kanton_zh')) :
        $html .= '<tr><td>' . get_sub_field('escort_service') . '</td><td class="text-right">' . get_sub_field('escort_preis') . '</td></tr>';
    endwhile;
    $html .= '</table>';
endif;


	$html .= '
				</div>

				<div role="tabpanel" class="tab-pane" id="escort-SZ">
					<h4>Preise Schwyz</h4>';

if(get_field('kanton_sz')) :
	$html .= '<table class="table table-striped" border="0">';
	while(the_repeater_field('kanton_sz')) :
        $html .= '<tr><td>' . get_sub_field('escort_service') . '</td><td class="text-right">' . get_sub_field('escort_preis') . '</td></tr>';
    endwhile;
    $html .= '</table>';
endif;


	$html .= '
				</div>

				<div role="tabpanel" class="tab-pane" id="escort-ZG">
					<h4>Preise Zug</h4>';

if(get_field('kanton_zg')) :
	$html .= '<table class="table table-striped" border="0">';
	while(the_repeater_field('kanton_zg')) :
        $html .= '<tr><td>' . get_sub_field('escort_service') . '</td><td class="text-right">' . get_sub_field('escort_preis') . '</td></tr>';
    endwhile;
    $html .= '</table>';
endif;


	$html .= '
				</div>

				<div role="tabpanel" class="tab-pane" id="escort-NW">
					<h4>Preise Nidwalden</h4>';

if(get_field('kanton_nw')) :
	$html .= '<table class="table table-striped" border="0">';
	while(the_repeater_field('kanton_nw')) :
        $html .= '<tr><td>' . get_sub_field('escort_service') . '</td><td class="text-right">' . get_sub_field('escort_preis') . '</td></tr>';
    endwhile;
    $html .= '</table>';
endif;


	$html .= '
				</div>

				<div role="tabpanel" class="tab-pane" id="escort-UR">
					<h4>Preise Uri</h4>';

if(get_field('kanton_ur')) :
	$html .= '<table class="table table-striped" border="0">';
	while(the_repeater_field('kanton_ur')) :
        $html .= '<tr><td>' . get_sub_field('escort_service') . '</td><td class="text-right">' . get_sub_field('escort_preis') . '</td></tr>';
    endwhile;
    $html .= '</table>';
endif;


	$html .= '
				</div>

				<div role="tabpanel" class="tab-pane" id="escort-OW">
					<h4>Preise Obwalden</h4>';

if(get_field('kanton_ow')) :
	$html .= '<table class="table table-striped" border="0">';
	while(the_repeater_field('kanton_ow')) :
        $html .= '<tr><td>' . get_sub_field('escort_service') . '</td><td class="text-right">' . get_sub_field('escort_preis') . '</td></tr>';
    endwhile;
    $html .= '</table>';
endif;


	$html .= '
				</div>

				<div role="tabpanel" class="tab-pane" id="escort-LU">
					<h4>Preise Luzern</h4>';

if(get_field('kanton_lu')) :
	$html .= '<table class="table table-striped" border="0">';
	while(the_repeater_field('kanton_lu')) :
        $html .= '<tr><td>' . get_sub_field('escort_service') . '</td><td class="text-right">' . get_sub_field('escort_preis') . '</td></tr>';
    endwhile;
    $html .= '</table>';
endif;


	$html .= '
				</div>

				<div role="tabpanel" class="tab-pane" id="escort-AG">
					<h4>Preise Aargau</h4>';

if(get_field('kanton_ag')) :
	$html .= '<table class="table table-striped" border="0">';
	while(the_repeater_field('kanton_ag')) :
        $html .= '<tr><td>' . get_sub_field('escort_service') . '</td><td class="text-right">' . get_sub_field('escort_preis') . '</td></tr>';
    endwhile;
    $html .= '</table>';
endif;


	$html .= '
				</div>

				<div role="tabpanel" class="tab-pane" id="escort-BL">
					<h4>Preise Baselland</h4>';

if(get_field('kanton_bl')) :
	$html .= '<table class="table table-striped" border="0">';
	while(the_repeater_field('kanton_bl')) :
        $html .= '<tr><td>' . get_sub_field('escort_service') . '</td><td class="text-right">' . get_sub_field('escort_preis') . '</td></tr>';
    endwhile;
    $html .= '</table>';
endif;


	$html .= '
				</div>

				<div role="tabpanel" class="tab-pane" id="escort-SO">
					<h4>Preise Solothurn</h4>';

if(get_field('kanton_so')) :
	$html .= '<table class="table table-striped" border="0">';
	while(the_repeater_field('kanton_so')) :
        $html .= '<tr><td>' . get_sub_field('escort_service') . '</td><td class="text-right">' . get_sub_field('escort_preis') . '</td></tr>';
    endwhile;
    $html .= '</table>';
endif;


	$html .= '
				</div>

				<div role="tabpanel" class="tab-pane" id="escort-JU">
					<h4>Preise Jura</h4>';

if(get_field('kanton_ju')) :
	$html .= '<table class="table table-striped" border="0">';
	while(the_repeater_field('kanton_ju')) :
        $html .= '<tr><td>' . get_sub_field('escort_service') . '</td><td class="text-right">' . get_sub_field('escort_preis') . '</td></tr>';
    endwhile;
    $html .= '</table>';
endif;


	$html .= '
				</div>

				<div role="tabpanel" class="tab-pane" id="escort-BE">
					<h4>Preise Bern</h4>';

if(get_field('kanton_be')) :
	$html .= '<table class="table table-striped" border="0">';
	while(the_repeater_field('kanton_be')) :
        $html .= '<tr><td>' . get_sub_field('escort_service') . '</td><td class="text-right">' . get_sub_field('escort_preis') . '</td></tr>';
    endwhile;
    $html .= '</table>';
endif;


	$html .= '
				</div>

				<div role="tabpanel" class="tab-pane" id="escort-NE">
					<h4>Preise Neuenburg</h4>';

if(get_field('kanton_ne')) :
	$html .= '<table class="table table-striped" border="0">';
	while(the_repeater_field('kanton_ne')) :
        $html .= '<tr><td>' . get_sub_field('escort_service') . '</td><td class="text-right">' . get_sub_field('escort_preis') . '</td></tr>';
    endwhile;
    $html .= '</table>';
endif;


	$html .= '
				</div>

				<div role="tabpanel" class="tab-pane" id="escort-FR">
					<h4>Preise Freiburg</h4>';

if(get_field('kanton_fr')) :
	$html .= '<table class="table table-striped" border="0">';
	while(the_repeater_field('kanton_fr')) :
        $html .= '<tr><td>' . get_sub_field('escort_service') . '</td><td class="text-right">' . get_sub_field('escort_preis') . '</td></tr>';
    endwhile;
    $html .= '</table>';
endif;


	$html .= '
				</div>

				<div role="tabpanel" class="tab-pane" id="escort-VD">
					<h4>Preise Waadt</h4>';

if(get_field('kanton_vd')) :
	$html .= '<table class="table table-striped" border="0">';
	while(the_repeater_field('kanton_vd')) :
        $html .= '<tr><td>' . get_sub_field('escort_service') . '</td><td class="text-right">' . get_sub_field('escort_preis') . '</td></tr>';
    endwhile;
    $html .= '</table>';
endif;


	$html .= '
				</div>

				<div role="tabpanel" class="tab-pane" id="escort-GE">
					<h4>Preise Genf</h4>';

if(get_field('kanton_ge')) :
	$html .= '<table class="table table-striped" border="0">';
	while(the_repeater_field('kanton_ge')) :
        $html .= '<tr><td>' . get_sub_field('escort_service') . '</td><td class="text-right">' . get_sub_field('escort_preis') . '</td></tr>';
    endwhile;
    $html .= '</table>';
endif;


	$html .= '
				</div>

				<div role="tabpanel" class="tab-pane" id="escort-VS">
					<h4>Preise Wallis</h4>';

if(get_field('kanton_vs')) :
	$html .= '<table class="table table-striped" border="0">';
	while(the_repeater_field('kanton_vs')) :
        $html .= '<tr><td>' . get_sub_field('escort_service') . '</td><td class="text-right">' . get_sub_field('escort_preis') . '</td></tr>';
    endwhile;
    $html .= '</table>';
endif;


	$html .= '
				</div>

				<div role="tabpanel" class="tab-pane" id="escort-TI">
					<h4>Preise Tessin</h4>';

if(get_field('kanton_ti')) :
	$html .= '<table class="table table-striped" border="0">';
	while(the_repeater_field('kanton_ti')) :
        $html .= '<tr><td>' . get_sub_field('escort_service') . '</td><td class="text-right">' . get_sub_field('escort_preis') . '</td></tr>';
    endwhile;
    $html .= '</table>';
endif;


	$html .= '
				</div>

				<div role="tabpanel" class="tab-pane" id="escort-GR">
					<h4>Preise Graubünden</h4>';

if(get_field('kanton_gr')) :
	$html .= '<table class="table table-striped" border="0">';
	while(the_repeater_field('kanton_gr')) :
        $html .= '<tr><td>' . get_sub_field('escort_service') . '</td><td class="text-right">' . get_sub_field('escort_preis') . '</td></tr>';
    endwhile;
    $html .= '</table>';
endif;


	$html .= '
				</div>

			</div>
		</div>
	</div>
	<hr>';

	return $html;
}
add_shortcode( 'escort_map', 'felice_sc_map' );


// HELPER: checks where the current visitor comes from 
// when found returns true then
function check_visitor_origin() {
	// prepare the locations you want to check for
	$origin['countries'] = [
	    ['Poland', 'PL'],
	    ['Hungary', 'HU'],
	    ['Germany', 'DE']
	];

	// get ip address
    $ip = getRealIpAddr();

    // request geo data
    foreach ($origin['countries'] as $o) {
        $result = unserialize(file_get_contents('http://www.geoplugin.net/php.gp?ip=' . $ip));

        // check for countryName
        if ($o[0] == $result['geoplugin_countryName']) {
            //echo 'visitor from '.$result['geoplugin_countryName'];
            return true;
        } 
		// if nothing found then check for countryCode
        elseif ($o[1] == $result['geoplugin_countryCode']) {
            //echo 'visitor from '.$result['geoplugin_countryCode'];
            return true;
        }
    } // end foreach

	// visitor is not in that country
    return false;
}

// HELPER: function used by check_visitor_origin()
function getRealIpAddr() {
    if (!empty($_SERVER['HTTP_CLIENT_IP'])) //check ip from share internet
    {
        $ip = $_SERVER['HTTP_CLIENT_IP'];
    } elseif (!empty($_SERVER['HTTP_X_FORWARDED_FOR'])) //to check ip is pass from proxy
    {
        $ip = $_SERVER['HTTP_X_FORWARDED_FOR'];
    } else {
        $ip = $_SERVER['REMOTE_ADDR'];
    }
    return $ip;
}

// Shortcode Map
function felice_block_content( $atts ) {
	// attributes
	extract(shortcode_atts(
		array(
			'content' => ''
		), $atts )
	);

	$html = '<div class="row">
		<div class="col-sm-3 text-center">
		    <p><img src="' . get_stylesheet_directory_uri() . '/img/flag-en.png"><br><br>Dear visitors of our website<br>
		    Out of consideration for our ladies unfortunately
		    Girls gallery is not enabled in your country.
		    Thank you for your understanding<br>
		    Villa-Felice Team</p>
		    <p>&nbsp;</p>
		</div>
		<div class="col-sm-3 text-center">
		    <p><img src="' . get_stylesheet_directory_uri() . '/img/flag-hu.png"><br><br>Kedves Látogató<br>
		    A Hölgyekre való tekintettel sajnos a lányok
		    képgaléria nincs engedélyezve az Ön országában.
		    Köszönjük a megértéstüket.<br>
		    Villa-Felice csapat</p>
		    <p>&nbsp;</p>
		</div>
		<div class="col-sm-3 text-center">
		    <p><img src="' . get_stylesheet_directory_uri() . '/img/flag-pl.png"><br><br>Szanowni Panstwo<br>
		    Z powodu dyskrecji dla dziewczyn u nas pracujacych,
		    galeria nie jest dostepna w danym kraju.
		    Dziękujemy za wyrozumiałość<br>
		    Villa-Felice Team</p>
		    <p>&nbsp;</p>
		</div>
		<div class="col-sm-3 text-center">
		    <p><img src="' . get_stylesheet_directory_uri() . '/img/flag-de.png"><br><br>Liebe Besucher unsere Homepage<br>
		    Aus Rücksicht zu unsere Damen ist leider die Girls
		    Galerie in ihrem Land nicht frei geschaltet.
		    Wir Bedanken uns für ihr Verständnis<br>
		    Villa-Felice Team</p>
		    <p>&nbsp;</p>
		</div>
	</div>';

	if (check_visitor_origin()) {
		return $html; // hide content, show info message
	} else {
		return $content; // display content
	}
}
add_shortcode( 'block_content', 'felice_block_content' );


// Echo preformatted debug message
function felice_debug( $var ) {
	echo '<pre>';
	print_r($var);
	echo '</pre>';
}