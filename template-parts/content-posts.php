<?php
/**
 * The template part for displaying posts
 */
?>

<article id="post-<?php the_ID(); ?>" <?php post_class(); ?>>
	<div class="row">
		<?php if ( has_post_thumbnail() ) { ?>
		<div class="col-md-5">
			<div class="img-cover"><?php the_post_thumbnail('large', array('class' => 'img-responsive')); ?></div>
			<small><?php echo get_post(get_post_thumbnail_id())->post_excerpt; ?></small>
		</div>
		<div class="col-md-7 post-text">
		<?php } else { ?>
		<div class="col-md-12 post-text">
		<?php } ?>
			<h3><?php the_title() ?></h3>
			<?php the_content(); ?>
		</div>
	</div>
</article><!-- #post-## -->
