<?php
/**
 * The template part for displaying blocked content
 */
?>

<div class="row">
	<div class="col-sm-3 text-center">
	    <p><img src="<?php echo get_stylesheet_directory_uri() . '/img/flag-en.png'; ?>"></p>
	    <?php if (get_field('blocked_message_en', 'options')) : ?>
	    	<?php the_field('blocked_message_en', 'options'); ?>
		<?php else : ?>
		    <p>Dear visitors of our website<br>
		    Out of consideration for our ladies unfortunately
		    Girls gallery is not enabled in your country.
		    Thank you for your understanding<br>
		    Villa-Felice Team</p>
		<?php endif; ?>
	    <p>&nbsp;</p>
	</div>
	<div class="col-sm-3 text-center">
	    <p><img src="<?php echo get_stylesheet_directory_uri() . '/img/flag-hu.png'; ?>"></p>
	    <?php if (get_field('blocked_message_hu', 'options')) : ?>
	    	<?php the_field('blocked_message_hu', 'options'); ?>
		<?php else : ?>
		    <p>Kedves Látogató<br>
		    A Hölgyekre való tekintettel sajnos a lányok
		    képgaléria nincs engedélyezve az Ön országában.
		    Köszönjük a megértéstüket.<br>
		    Villa-Felice csapat</p>
		<?php endif; ?>
	    <p>&nbsp;</p>
	</div>
	<div class="col-sm-3 text-center">
	    <p><img src="<?php echo get_stylesheet_directory_uri() . '/img/flag-pl.png'; ?>"></p>
	    <?php if (get_field('blocked_message_pl', 'options')) : ?>
	    	<?php the_field('blocked_message_pl', 'options'); ?>
		<?php else : ?>
		    <p>Szanowni Panstwo<br>
		    Z powodu dyskrecji dla dziewczyn u nas pracujacych,
		    galeria nie jest dostepna w danym kraju.
		    Dziękujemy za wyrozumiałość<br>
		    Villa-Felice Team</p>
		<?php endif; ?>
	    <p>&nbsp;</p>
	</div>
	<div class="col-sm-3 text-center">
	    <p><img src="<?php echo get_stylesheet_directory_uri() . '/img/flag-de.png'; ?>"></p>
	    <?php if (get_field('blocked_message_de', 'options')) : ?>
	    	<?php the_field('blocked_message_de', 'options'); ?>
		<?php else : ?>
		    <p>Liebe Besucher unsere Homepage<br>
		    Aus Rücksicht zu unsere Damen ist leider die Girls
		    Galerie in ihrem Land nicht frei geschaltet.
		    Wir Bedanken uns für ihr Verständnis<br>
		    Villa-Felice Team</p>
		<?php endif; ?>
	    <p>&nbsp;</p>
	</div>
</div>