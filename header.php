<?php
/**
 * The template for displaying the header
 */
setlocale (LC_ALL, 'de_DE');
?><!DOCTYPE html>
<html <?php language_attributes(); ?> class="no-js">
<head>
	<meta charset="<?php bloginfo( 'charset' ); ?>">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<link rel="profile" href="http://gmpg.org/xfn/11">
	<?php if ( is_singular() && pings_open( get_queried_object() ) ) : ?>
	<link rel="pingback" href="<?php bloginfo( 'pingback_url' ); ?>">
	<?php endif; ?>

	<title><?php wp_title( '|', true, 'right' ); ?><?php echo bloginfo('name'); ?> – <?php echo bloginfo('description'); ?></title>

	<!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
	<!--[if lt IE 9]>
	  <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
	  <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
	<![endif]-->

	<?php wp_head(); ?>
	<script>
	(function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
	(i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
	m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
	})(window,document,'script','https://www.google-analytics.com/analytics.js','ga');
	ga('create', 'UA-91486132-1', 'auto');
	ga('send', 'pageview');
	</script>
</head>

<body <?php body_class(); ?>>
	<div class="container" id="topline">
		<div class="social-media pull-left">
			<ul class="list-inline">
				<?php if (get_field('facebook', 'option')) : ?><li><a href="<?php the_field('facebook', 'option'); ?>" target="_blank"><i class="fa fa-facebook-square"></i></a></li><?php endif; ?>
				<?php if (get_field('youtube', 'option')) : ?><li><a href="<?php the_field('youtube', 'option'); ?>" target="_blank"><i class="fa fa-youtube-square"></i></a></li><?php endif; ?>
				<?php if (get_field('vimeo', 'option')) : ?><li><a href="<?php the_field('vimeo', 'option'); ?>" target="_blank"><i class="fa fa-vimeo-square"></i></a></li><?php endif; ?>
				<?php if (get_field('twitter', 'option')) : ?><li><a href="<?php the_field('twitter', 'option'); ?>" target="_blank"><i class="fa fa-twitter-square"></i></a></li><?php endif; ?>
				<?php if (get_field('instagram', 'option')) : ?><li><a href="<?php the_field('instagram', 'option'); ?>" target="_blank"><i class="fa fa-instagram"></i></a></li><?php endif; ?>
				<?php if (get_field('googleplus', 'option')) : ?><li><a href="<?php the_field('googleplus', 'option'); ?>" target="_blank"><i class="fa fa-google-plus-square"></i></a></li><?php endif; ?>
			</ul>
		</div>
		<div class="phone pull-right">
			<?php if (get_field('email', 'option')) : ?><a href="mailto:<?php the_field('email', 'option'); ?>"><i class="fa fa-envelope-square"></i> <?php the_field('email', 'option'); ?></a><?php endif; ?>
			<?php if (get_field('phone', 'option')) : ?><a href="tel:<?php echo str_replace(' ', '', get_field('phone', 'option') ); ?>"><i class="fa fa-phone-square"></i> <?php the_field('phone', 'option'); ?></a><?php endif; ?>
		</div>
	</div>
	<div class="container-fluid" id="headerfullwidth">
		<div class="header-background">
			<div class="container">
				<div class="row">
					<div class="col-xs-8 col-sm-6 col-md-5">
						<div class="logo">
							<a href="<?php echo bloginfo('url'); ?>"><img alt="<?php echo get_bloginfo('name'); ?>" src="<?php the_field('logo', 'option'); ?>" class="img-responsive"></a>
							<h3><span>Die neue Erotik-Villa</span><br>in Zürich &amp; Umgebung</h3>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>

	<div class="container">

		<?php if ( has_nav_menu( 'main' ) ) : ?>
		<nav class="navbar navbar-default" id="navigation">
			<div class="navbar-header">
				<button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#navbar-collapse-1" aria-expanded="false">
					<span class="sr-only">Toggle navigation</span>
					<span class="icon-bar"></span>
					<span class="icon-bar"></span>
					<span class="icon-bar"></span>
				</button>
			</div>

			<div class="collapse navbar-collapse" id="navbar-collapse-1">
				<?php
					wp_nav_menu( array(
						'theme_location' => 'main',
						'menu_class'     => 'nav nav-justified',
					 ) );
				?>
			</div>
		</nav>
		<?php endif; ?>
